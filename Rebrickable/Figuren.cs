﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rebrickable
{
    class Figuren
    {

        private string mSet_num;
        private string mName;
        private int mNum_parts;
        private string mSet_img_url;
        private string mSet_url;
        private string mLast_modified_dt;


        public string set_num
        {
            get { return mSet_num; }
            set { mSet_num = value; }
        }

        public string name
        {
            get { return mName; }
            set { mName = value; }
        }

        public int num_parts
        {
            get { return mNum_parts; }
            set { mNum_parts = value; }
        }

        public string set_img_url
        {
            get { return mSet_img_url; }
            set { mSet_img_url = value; }
        }

        public string set_url
        {
            get { return mSet_url; }
            set { mSet_url = value; }
        }

        public string last_modified_dt
        {
            get { return mLast_modified_dt; }
            set { mLast_modified_dt = value; }
        }

    }
}
