﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using RestSharp;
using Newtonsoft.Json;

namespace Rebrickable
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HttpClient client = new HttpClient();
        private string ApiLink = "https://rebrickable.com/api/v3/lego/minifigs?key=47fe1eae4a73a1838576ba223cc5cdb9&page=1";
        private Data infoFiguren;

        public MainWindow()
        {
            InitializeComponent();
            lblWebsiteLink.Visibility = Visibility.Hidden;
          
            GetItems();
        }


        // test API https://jsonplaceholder.typicode.com/todos
        private async void GetItems()
        {
            var response = await client.GetStringAsync(ApiLink);

            infoFiguren = JsonConvert.DeserializeObject<Data>(response);


            foreach (var item in infoFiguren.Results)
            {
                cbNamen.Items.Add(item.name);
            }

        } // get the first page when you start up

        private async void GetNextPage(int next)
        {
            cbNamen.SelectedIndex = -1;
            cbNamen.Items.Clear();
            txtNr.Text = "";
            txtParts.Text = "";
            txtEdited.Text = "";
            lblWebsiteLink.Visibility = Visibility.Hidden;

            string ApiLinkKeyAndPage = ApiLink.Substring(0, 87);
            string ApiLinkPageNr = ApiLink.Substring(87);


            int NextApiLinkPageNr = Convert.ToInt32(ApiLinkPageNr) + next;

            if (NextApiLinkPageNr == 1)
            {
                btnVorigeLijst.IsEnabled = false;
            }
            else
            {
                btnVorigeLijst.IsEnabled = true;
            }

            if (NextApiLinkPageNr == 121) // There only are 121 pages
            {
                btnVolgendeLijst.IsEnabled = false;
            }
            else
            {
                btnVolgendeLijst.IsEnabled = true;
            }


            ApiLink = ApiLinkKeyAndPage + NextApiLinkPageNr.ToString();

            var response = await client.GetStringAsync(ApiLink);

            infoFiguren = JsonConvert.DeserializeObject<Data>(response);

            lblPaginaNr.Content = "Pagina nummer: " + NextApiLinkPageNr.ToString();

            foreach (var item in infoFiguren.Results)
            {
                cbNamen.Items.Add(item.name);
            }
        } // go to the next or previous page

        private void cbNamen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lblWebsiteLink.Foreground = Brushes.DarkBlue;
            lblWebsiteLink.Visibility = Visibility.Visible;

            BitmapImage afbeelding;

            if (cbNamen.SelectedIndex == -1)
            {
                afbeelding = new BitmapImage(new Uri("", UriKind.RelativeOrAbsolute));
                Img.Source = afbeelding;
            }
            else
            {
                foreach (var item in infoFiguren.Results)
                {

                    if (cbNamen.SelectedItem.ToString() == item.name)
                    {

                        try
                        {
                            afbeelding = new BitmapImage(new Uri(item.set_img_url, UriKind.RelativeOrAbsolute));
                        }

                        catch (ArgumentNullException)
                        {
                            afbeelding = new BitmapImage(new Uri("icons8-lego_head.jpg", UriKind.RelativeOrAbsolute)); // figuur 101 doesn't have a picture
                        }

                        Img.Source = afbeelding;

                        txtNr.Text = item.set_num;
                        txtParts.Text = item.num_parts.ToString();

                        string datum = item.last_modified_dt.Substring(0, 10);
                        string tijd = item.last_modified_dt.Substring(11, 8);

                        txtEdited.Text = datum + " - " + tijd;
                    }

                }
            }  

        } // get all the information when you select a different figur

        private void Img_MouseDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var item in infoFiguren.Results)
            {
                if (cbNamen.SelectedItem.ToString() == item.name)
                {
                    try
                    {
                        System.Diagnostics.Process.Start(item.set_img_url); // openen van webpagina 
                    }
                    catch (System.InvalidOperationException)
                    {
                        MessageBox.Show("Deze figuur heeft geen afbeelding.", "Geen foto", MessageBoxButton.OK, MessageBoxImage.Information); // figuur 101 heeft geen foto
                    }
                    
                }
            }
        } //  open a webpage with the picture in it

        private void Img_MouseEnter(object sender, MouseEventArgs e)
        {
            Img.Opacity = 0.75;
            Cursor = Cursors.Hand;
        } // make the look clickable for lblWebsiteLink 

        private void Img_MouseLeave(object sender, MouseEventArgs e)
        {
            Img.Opacity = 1;
            Cursor = Cursors.Arrow;
        } // return to normale look img

        private void lblWebsiteLink_MouseDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var item in infoFiguren.Results)
            {
                if (cbNamen.SelectedItem.ToString() == item.name)
                {
                    System.Diagnostics.Process.Start(item.set_url); // openen van webpagina
                }
            }
        } //  open a webpage with more info about the selected figure

        private void lblWebsiteLink_MouseEnter(object sender, MouseEventArgs e)
        {
            lblWebsiteLink.Foreground = Brushes.LightBlue;
            Cursor = Cursors.Hand;
        }   // make the look clickable for lblWebsiteLink

        private void lblWebsiteLink_MouseLeave(object sender, MouseEventArgs e)
        {
            lblWebsiteLink.Foreground = Brushes.DarkBlue;
            Cursor = Cursors.Arrow;
        } // return to normale look lblWebsiteLink

        private void btnVorigeLijst_Click(object sender, RoutedEventArgs e)
        {
            GetNextPage(-1);
        } // previous page 

        private void btnVorigeLijst_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
        } // show you can click the button

        private void btnVorigeLijst_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
        } // get mouse back to  normal

        private void btnVolgendeLijst_Click(object sender, RoutedEventArgs e)
        {
            GetNextPage(1);
        } // next page

        private void btnVolgendeLijst_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Hand;
        } // show you can click the button

        private void btnVolgendeLijst_MouseLeave(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.Arrow;
        } // get mouse back to  normal
    }
}
